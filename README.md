# Evochron Mercenary - Mod FR

## Descriptif :

Ce mod traduit en français plusieurs éléments du jeu et applique quelques modifications graphiques pour une meilleure intégration.


## Installation :

1. Copier tous les fichiers dans le dossier d'installation de Evochron Mercenary
(ex : "C:\Program Files\Steam\steamapps\common\Evochron Mercenary" ou "C:\sw3dg\EvochronMercenary")

2. Installer/Mettre à jour la police de caractère (font) "EvochronFontFr"
(Clic droit sur le fichier "EvochronFontFr.ttf" puis choisir "Installer")


## Contributions :

* Fhamtaom
* yannou1082
* Maximilien
* Petite_Fraise
* Et toute la communauté d'Evochron Mercenary