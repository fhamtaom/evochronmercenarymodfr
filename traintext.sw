-1
9
Bienvenue � l'entrainement aux bases du pilotage, du 
combat, et du contr�le des syst�mes. Cette s�rie de 
session d'entrainement vous fournira des instructions 
d�taill�es pour apprendre � contr�ler votre vaisseau, 
g�rer les syst�mes, naviguer, commercer et utiliser 
les armes. Vous pouvez sauter n'importe quel phase de 
l'entrainement en appuyant sur 'Entr�e'. Vous pouvez 
�galement retourner � la le�on pr�c�dente en appuyant 
sur ALT-Entr�e.

-2
8
Pour cette premi�re le�on, nous allons couvrir les bases
du pilotage. Le contr�leur de vol par d�faut est la 
souris si aucun joystick n'a pu �tre d�tect�. Vous 
pouvez modifier les options de contr�le dans le menu 
Option. Pour le contr�le avec la souris vous pouvez 
choisir entre le mode pointage ou direction. Le mode 
direction vous permet un contr�le direct en d�pla�ant 
la souris.

-3
7
Commen�ons avec le contr�le de la vitesse. Si vous 
utilisez la souris, vous pouvez l'ajuster avec la 
molette, ou avec les touches 1 � 0 du clavier pour 
r�gler un pourcentage. ) et = ou z et s permettent 
aussi le r�glages de la vitesse. L'indicateur VEL 
indique votre vitesse actuelle. Augmentez votre 
vitesse au maximum maintenant.

-4
3
Bien, maintenant r�glez votre vitesse sur 0. Si 
vous utilisez le clavier, la touche Effacer vous 
permet d'arr�ter le moteur imm�diatement.

-5
7
En plus de votre r�acteur principal, votre vaisseau 
est �quip� de propulseurs r�partis sur la coque. Ces 
r�acteurs sont contr�l�s automatiquement par 
l'ordinateur de bord pour vous aider � contr�ler votre
direction. Mais vous pouvez aussi les actionner 
manuellement. Utilisez votre contr�leur pour tourner 
� gauche, puis � droite.

-6
1
Maintenant, vers le haut puis le bas.

-7
4
Bien. Le contr�le suivant est la rotation. Les 
touches par d�faut sont A et E pour effectuer ces 
man�uvres. Effectuez une rotation vers la gauche, 
puis vers la droite.

-8
8
En plus des commandes de rotations et de vitesse, vous 
pouvez �galement vous d�placer vers le haut, le bas, la 
gauche ou la droite en utilisant les propulseurs verticaux 
et horizontaux. Les commandes par d�faut sont Q et D 
pour les mouvements horizontaux, W et X pour les mouvements 
verticaux. Si vous utilisez un joystick, vous pouvez r�gler 
jusqu'� six axes de d�placement. Essayez de vous d�placer 
horizontalement (Q et D).

-9
1
Maintenant, vers le haut puis le bas (W et X).

-10
12
Bien jou�. Voler dans l'espace demande une ma�trise des 
effets de l'inertie. Le contr�le automatique d'inertie (IDS) 
vous aide � maitriser votre vaisseau en d�pit de l'inertie, 
et � toujours vous d�placer en faisant face � votre 
direction. Vous pouvez cependant d�sactiver ce syst�me gr�ce 
� la touche Espace du clavier. Cela vous permet de prendre 
une direction et de la conserver tout en modifiant votre axe 
de vue. Le statut du contr�leur inertiel apparait sur votre 
�cran de contr�le. Entrainez-vous � prendre de la vitesse en 
mode IDS, puis d�sactivez le et essayez de tourner dans 
diff�rentes directions pour comprendre le fonctionnement 
de l'inertie.

-11
2
R�activez votre IDS pour r�activer le contr�le automatique 
de vos propulseurs.

-12
9
Le syt�me IDS poss�dent 5 niveaux de puissance, le niveau
actuel est affich� � droite de l'indicateur IDS, en bas 
� gauche du cockpit. x1 est le niveau par d�faut et est 
le plus adapt� aux changement de cap rapide n�cessaire 
en combat ou en accostage. x5 est le niveau maximum mais
votre vaisseau prendra plus de temps pour changer de cap.
Essayez de modifier cette valeur avec les touches 7 et 9 du 
pav� num�rique afin d'en comprendre l'impact sur les manoeuvres.
Mettez � nouveau la valeur sur 1 lorsque vous avez termin�.

-13
8
Votre vaisseau est aussi �quip� d'une postcombustion
qui procure une propulsion de grande puissance gr�ce 
aux moteurs principaux. Son utilisation consomme beaucoup 
de carburant, mais est tr�s utile en combat pour 
s'�chapper. Pour activer la postcombustion pressez 
la touche Tab. Entrainez-vous en doublant la vitesse 
maximum du vaisseau puis rel�chez la touche Tab pour 
d�sactiver la postcombustion.

-14
9
L'entrainement aux contr�les de vol est maintenant 
termin�. Nous allons maintenant voir le syst�me 
suivant. Les statuts de combat et d'environnement 
s'affichent sur votre cockpit (HUD) sur trois 
�crans distincts. L'HUD inclus certaines informations 
optionnelles comme la vitesse, l'altitude, la gravit�. 
Le HUD comprend trois modes qui permettent d'afficher 
plus ou moins d'information. La touche par d�faut pour 
changer d'affichage est H.

-15
13
Votre vaisseau r�partit son �nergie entre les armes et 
le syst�me de bouclier. Le r�glage en cours est affich�
dans l'�cran de gauche en bas du cockpit, sur le coin 
droit, en haut � c�t� de PWR. L'�tendu du r�glage de 
chaque syst�me va de -5 � 5. Shield d�signe le 
bouclier et WPN les armes. Vous ajusterez le niveau de 
r�partition de l'�nergie en utilisant les touches 
^ et $. Augmenter un des syst�mes diminuera son temps 
de recharge et augmentera son efficacit�. La gestion de 
l'�nergie deviendra importante surtout lorsque vous 
am�liorerez vos syst�mes d'armes et de bouclier. Juste � 
gauche de l'indicateur de niveau de puissance est le nombre 
de contre-mesures que vous poss�dez (CM).

-16
7
WPN indique le niveau de puissance du syst�me d'armes. 
La barre bleue va diminuer lorsque vous tirez avec 
vos armes principale. Une fois que la barre atteind
le centre, votre syst�me d'armement n'aura plus de puissance
et devra se recharger. Observez cela en tirant avec votre
arme principale jusqu'� ce que la barre indique un faible 
niveau de puissance. La touche par d�faut est CTRL gauche.

-17
16
Bien. L'indicateur vous a indiqu� un niveau d'�nergie
faible. Sous la barre d'�nergie des armes, ce trouve une 
barre verte appel�e HUL. Celle-ci indique l'�tat 
de la coque du vaisseau. Cette barre diminue lorsque la 
coque subit des dommages. Le blindage de votre vaisseau est 
d�termin� par la frame que vous aurez choisi pour sa 
construction. Sur le c�t� gauche de l'�cran de contr�le 
se trouve diverses informations comme la v�locit� (VEL), la 
vitesse param�tr�e (SET), la gravit� (GRV), l'altitude (ALT), 
le status in�rtiel(IDS), le carburant, les dommages 
du syst�me de navigation (NAV), les dommages du syst�me 
d'armement(WPN), les dommages moteur (ENG), l'�tat 
des boucliers, et le mode de tir de l'arme principale (MODE). 
Vos armes secondaires sont list�es dans la partie droite. 
L'arme secondaire situ�e sur le chiffre 1 est la prochaine 
qui sera utilis�e.

-18
13
L'�cran du milieu est votre radar 3d. Il fournit des 
informations sur la positions des objets dans l'espace. 
Les vaisseaux amis apparaissent en vert, les neutres en 
jaune et les ennemis en rouge. Lorsqu'un point rouge apparait 
sur le radar, un avertisseur sonore attirera votre attention,
Notez que les plus petit points rouge indiquent les missiles. 
Les points bleus indiquent les stations, les blancs les cargos, 
et les violets indiquent les objets non identifi�s. 
Les points affichent une ligne depuis le centre vous indiquant
si l'objet se trouve devant ou derri�re vous. L'indicateur bleu
au centre de la sph�re correspond � votre vaisseau.
Directement au-dessus du radar se trouvent les barres indiquant 
les d�placements horizontaux et verticaux.


-19
15
L'�cran en bas � droite fournis des informations sur le 
vaisseau s�lectionn� par le syst�me de vis�e automatique. 
Pour s�lectionner un vaisseau, cliquez simplement sur lui 
lorsqu'il est dans votre champ de vision. Vous passez 
d'une cible a l'autre en utilisant la T ou R pour 
s�lectionner le vaisseau le plus proche. ALT-T basculera
entre les vaisseaux ennemis dans la zone tandis qu'ALT-R
ciblera le vaisseau ennemi le plus proche. Vous pouvez 
�galement cibler manuellement un vaisseau avec la touche Y.
Lorsqu'un vaisseau est s�lectionn�, sa cat�gorie (ID), 
son affiliation � une faction, son agressivit� (THR), 
sa vitesse (VEL), sa distance (RNG), le niveau 
de ses boucliers, sa direction et l'�tat de la coque sont 
affich�s. Si vous avez un scanner de soute, le contenu 
de celle-ci sera affich� si vous �tes a port�e de scan.

-20
19
Le centre de l'HUD est la mire de votre vaisseau. Le cercle 
ext�rieur indique la zone ou vous pouvez obtenir un 
verrouillage missile et la zone dans laquelle l'arme 
principale peut s'orienter automatiquement vers la cible. 
L'arme principale utilise le syst�me de suivi multi-directionnel 
(MDTS) pour s'orienter automatiquement vers la cible
en se basant sur sa vitesse, sa direction et sa distance. 
La cible doit �tre � port�e et �tre situ�e � l'int�rieur du 
cercle ext�rieur pour pouvoir �tre touch�e. Plus la cible
est �loign�e, plus elle dispose de temps pour s'�chapper 
du feu de vos cannons. Si vous voulez cibler manuellement, 
vous pouvez d�sactiver le MDTS avec la touche L. Le 
v�rrouillage missile s'obtient en conservant la cible un 
certain temps � l'int�rieur du cercle ext�rieur. Ce temps 
et la port�e du verrouillage d�pend du type d'arme 
secondaire install�. Certains missiles comme les torpilles 
gravitationnelle ne demandent pas de verrouillage, elles 
partent simplement en ligne droite. Vous pouvez basculer 
entre les armes secondaires avec les touches m et �.

-21
3
Exercez-vous au combat avec ce drone (son niveau 
d'agressivit� sera r�gl� sur �lev�, en rouge) 
jusqu'� sa destruction.

-22
9
Bien. Au dessus du viseur se trouve l'indicateur de
la signature thermique �mis par votre vaisseau.
Lorsque vous manoeuvrez ou changez de vitesse, vos
moteurs et propulseurs produisent de la chaleurs
permettant aux missiles ennemi de vous v�rouiller
et de vous suivre, ce qui rendra vos contre-mesure
moins efficace. Utilisez cet indicateur pour en r�duire
le niveau afin d'augmenter l'efficacit� de vos CM si
vous avez besoin de vous �chapper. 

-23
15
Vous pouvez installer deux types d'armes principales 
sur votre vaisseaux. Les armes � particules tirent des 
projectiles � hautes v�locit� et sont g�n�ralement peu 
efficace contre les boucliers mais d�vastatrices sur les 
blindages. Chaque type d'armes � particules poss�de une 
port�e, une puissance et une cadence de tir propre. 
Certaines armes � particules sont optimis�es pour drainer 
les boucliers, l'�nergie ou infliger des d�g�ts cin�tiques. 
Il faut donc conna�tre les caract�ristiques de chaque arme 
pour bien en comprendre l'utilisation. Les canons � faisceau
sont g�n�ralement plus efficace sur les boucliers mais utilisent
plus d'�nergie. Vous pouvez choisir le type de canon � utiliser, 
individuellement ou simultan�, en changeant le mode de tir � 
l'aide de la touche N. Vous pouvez �galement utiliser les touches
F5 pour les canons � particule et F6 pour les canons � faiseau.

-24
11
Quand un missile vous prend pour cible, vous pouvez choisir 
entre fuir ou le d�truire avec vos armes. Esquiver un missile 
est une comp�tence importante � maitriser. Beaucoup de 
missiles vous traquent gr�ce � votre signature thermique, 
facilement identifiable dans le froid de l'espace, surtout 
avec vos r�acteurs. Vous pouvez r�duire significativement 
votre signature thermique en coupant l'IDS et en contr�lant 
les man�uvres manuellement. Dans ce cas vos contre-mesures 
seront plus efficaces. Pour lancer vos contre-mesures 
attendez que le missile soit suffisamment proche pour d�clencher 
l'alarme de proximit�.

-25
9
Un des aspects les plus important lors des combats est la 
gestion des boucliers. Votre syst�me de bouclier projette 
des champs r�pulsifs dans 4 directions. Si un des champs 
est affaibli, les tirs ennemis pourront atteindre votre 
coque et causer des dommages. Le niveau de chacune des 
directions est indiqu� sur votre HUD comme mentionn� 
pr�c�demment. Les barres vertes repr�sentent l'�tat de 
chacun des champs de votre bouclier. 
Regardez le bouclier avant chuter � son niveau minimum.

-26
8
La barre verte se r�duit et vire au rouge. Lors de la 
recharge des boucliers, celle-ci deviendra jaune puis 
verte � nouveau. Vous pouvez transf�rer l'�nergie d'un 
des champs vers les autres. Nous allons nous entrainer 
� cela. Votre bouclier avant est dans le rouge. Pour 
l'amplifier au d�triment des autres, vous pouvez utiliser 
la touche 8 du pave num�rique. Les boucliers arri�re, 
gauche et droit seront drain�s vers l'avant.

-27
7
Bien. En plus du transfert de puissance vers un bouclier 
sp�cifique, vous pouvez aussi �quilibrer tous les 
boucliers d'un coup en utilisant la touche 5 du pave
num�rique. Souvenez-vous que vous pouvez augmenter 
l'�nergie du bouclier en transf�rant de la puissance 
en provenance du syst�me d'armement pour r�duire le 
temps de recharge.

-28
14
La partie haute du HUD se compose d'un compas et de divers 
menu d'options. Pour s�lectionner un menu, utilisez 
simplement la souris et cliquez sur le boutons d�sir�. 
Si vous utilisez la souris comme moyen de contr�le du 
vaisseau, vous pouvez suspendre le contr�le en pressant 
la touche ALT et en la maintenant enfonc�e. Le menu  
Target vous permet de s�lectionner une cible ou de changer 
le statut du lien de clan d'un autre joueur en multi-joueurs. 
Le menu Ordre vous permet d'envoyer diff�rents ordres a 
votre flotte. En mode solo, les ordres seront 
donn�s aux vaisseaux contr�l�s par l'ordinateur qui ont 
rejoint votre flotte. En mode multi-joueurs, les ordres 
seront donn�s � tous les joueurs poss�dant le m�me tag 
de clan que vous.

-29
11
Sur la partie droite des menus, vous trouverez les menus 
Deploy et Build. Le menu Deploy vous permet 
de d�ployer du mat�riel temporaire qui vous permettra 
de recharger votre vaisseau, d�tecter des objets ou des 
vaisseaux � grande distance ou fournir un bouclier. Le 
menu Build vous permet de construire des stations � 
long terme dans de nouvelles zones, et d'�tendre le jeu 
en introduisant de nouvelles options de commerce, des 
vaisseaux contr�l�s par l'ordinateur et des nouveaux 
march�s. Vous devez avoir l'�quipement ad�quat pour 
d�ployer ou construire.

-30
8
Au milieu du haut du HUD vous aurez acc�s aux consoles, 
au pilote automatique, aux options de formations et � 
l'hyperdrive. Cliquez sur AUTO pour engager le pilote 
automatique. Le pilote automatique prend le contr�le 
de votre vaisseau et l'am�ne au point de navigation 
actif. Le point de destination NAV apparait sur votre HUD 
et sur le radar avec un marqueur jaune. 
Activez le pilote automatique maintenant.

-31
6
Constatez comment votre vaisseau change de direction et 
voyage directement vers la destination (marqueur NAV). Le 
pilote automatique contr�le �galement les sauts en 
cr�ant des puits gravitationnels si la destination est 
en dehors du secteur actuel. Maintenant, d�sativez
le pilote automatique.

-32
3
Le bouton FORM sur la droite de la boussole fonctionne 
de la m�me mani�re et met votre vaisseau en formation serr�e 
avec le vaisseau s�lectionn�.

-33
7
Il existe 3 consoles principales utilis�es pour la 
navigation et le commerce. La premi�re est la console 
de navigation qui s'ouvre en cliquant sur le bouton NAV 
ou avec la touche de raccourci F1. Si vous contr�lez le 
vol avec la souris, restez appui� sur ALT pour d�placer 
le curseur sur le bouton NAV.
Ouvrez la console de navigation maintenant. 

-34
14
Cette console vous permet de g�rer vos sauts 
(hyperdrive) et votre interface de navigation. 
Votre position actuelle est affich�e sur la carte 
par un symbole vert. Vous pouvez aussi voir diff�rents 
objets comme des champs d'ast�ro�des ou des plan�tes 
sur cette m�me carte. La carte divise les syst�mes en 
cube appel�s secteurs. Chaque secteur utilise une 
distance de -100 000 � +100 000 d'un c�t� � l'autre
dans toute les directions. Sur la vue par d�faut 
(Top View), les coordon�es X se lisent � l'horizontal
et Z � la verticale. Le point central repr�sente le 
centre du secteur localis� en X0 et Z0. Vous pouvez
cliquer sur Rear View pour retourner la carte
afin de voir le secteur par l'arri�re.

-35
8
Dans ce mode, les lignes verticale repr�sentent l'axe Y.
Pour s�l�ctionner un nouveau point de navigation il
suffit de cliquer avec le bouton gauche sur l'emplacement
souhait�. Sur la vue de dessus (Top View), s�l�ctionner 
un point de navigation d�finira les coordonn�es sur les
axes X et Z tandis que sur la vue arri�re (Rear View),
il se d�finira sur les axes X et Y. Cliquez maintenant
sur Top View pour revenir � l'orientation par d�faut.

-36
8
Pour d�finir un nouveau point de 
navigation, cliquez simplement sur la destination avec 
la souris. Les coordonn�es X et Z seront automatiquement 
d�finies et vous ajuster les coordonn�es Y en utilisant 
la vue arri�re (sur la droite de la carte) ou en 
entrant manuellement une valeur. Entrainez-vous en 
d�finissant un point proche des coordonn�es 
suivantes : X=40 000 et Z=40 000.

-37
5
Cliquez maintenant sur LAUNCH ou appuyez sur F2 
pour ouvrir un puits gravitationnel et op�rer un saut 
vers cette destination. Si la console est ferm�e, 
vous pouvez aussi utiliser le bouton JUMP du HUD.
Vous pouvez fermer la console quand vous le d�sirez.

-38
17
Vous �tes arriv� dans la zone que vous avez s�lectionn�e.
Les coordonn�es pour les secteurs sont indiqu�es avec 
SX, SY et SZ. Le secteur o� vous vous trouvez se situe 
au milieu du quadrant et indiquent SX 0, SY 0 et SZ 0.
Vous pouvez voir ces valeurs dans le console de 
navigation � Current Position en haut � gauche de 
l'�crans. Les fronti�re de secteur sont indiqu�es en 
blanc, vous pouvez voyager vers les autres secteurs de 
la m�me mani�re. Cliquez simplement sur Zoom Out ou 
avec la molette de la souris pour voir une zone plus 
vaste et cliquer sur une destination. Si vous voulez 
zoomer sur un secteur proche, cliquez simplement avec 
le bouton droit de la souris sur le secteur d�sir�. 
Vous pouvez aussi utiliser la molette de la souris pour 
zoomer ou d�zoomer depuis l'emplacement de votre 
vaisseau sur la carte. Effectuez maintenant un zoom 
arri�re pour afficher plus de secteurs.

-39
9
Voyez que lorsque vous zoomer, plus de secteurs 
apparaissent tout comme les objets dans ces secteurs. 
Vous pouvez zoomer sur ces secteurs avec un clic droit. 
Essayez maintenant en zoomant sur le secteur en haut � 
droite du secteur o� vous vous situez actuellement. 
Les coordonn�es de ce secteur sont SX 1, SY 0 et SZ 1. 
Pour vous aider, les coordoon�es secteur d'o� se trouve 
le pointeur de la souris sont indiqu�es surle c�t� gauche 
de la console de navigation.

-40
7
Si vous avez besoin de plus de pr�cision ou d'une 
meilleur visibilit� pour les textes et les ic�nes, 
cliquez sur le bouton Zoom map dans le coin inf�rieur 
droit de la console. Cela permet de doubler la taille 
de la carte et de retirer divers �l�ments graphique pour 
avoir une vue plus vaste et d�gag�e. Essayez ceci en 
cliquant sur le bouton Zoom map maintenant.

-41
3
Un zoom eff�ctu� avec la molette de la souris fonctionne
de la m�me mani�re dans ce mode. Cliquez maintenant sur 
le bouton Normal Map pour revenir � la vue par d�faut.

-42
8
La console de navigation offre �galement la possibilit� 
de d�placer la carte avec le pointeur de la souris. 
Testez cette fonctionnalit� maintenant en cliquant sur 
le bouton Mode Slide dans le coin inf�rieur droit puis 
faites glisser la carte en maintenant le bouton gauche 
de la souris. Si le mode Slide n'est pas visible, faites 
un zoom � l'aide de la molette de la souris ou du bouton 
Zoom avant jusqu'� ce que l'option apparaisse.

-43
6
Notez que vous pouvez faire d�filer la carte � n'importe 
quel endroit dans la zone du capteur. Si vous voulez 
v�rrouiller un secteur pour tracer des points de navigation 
lorque vous utiliser le mode Slide, cliquez sur le bouton 
Select Mode pour que le secteur s'affiche en int�gralit�. 
Essayez maintenant en cliquant sur Select mode.

-44
16
Si votre moteur de saut peut atteindre un point �loign� 
vous serez en mesure de le joindre rapidement. Si la 
distance est au del� des capacit�s du moteur, le pilote 
automatique engagera les sauts autant de fois qu'il sera 
n�cessaire jusqu'� joindre le point d�fini. Vous pourrez 
am�liorer votre moteur de saut (Fulcrum Drive) pour 
augmenter la distance des bond que votre vaisseau pourra 
effectuer. Vous pouvez enregistrer des positons 
importantes dans le journal (LOG) et y entrer une 
description pour vous souvenir de son int�r�t. Si vous 
jouez en ligne, vous pouvez utiliser le bouton Ping pour 
signaler votre position aux joueurs du m�me secteur. 
Quand vous cliquez sur le bouton, une case verte apparait 
sur leur carte de navigation indiquant votre positon. 
Vous pouvez �galement indiquer votre position aux joueurs 
d'autres secteurs avec le bouton Trans Pos disponible 
en multi-joueur.

-45
5
De la m�me mani�re qu'avec le zoom secteur, vous pouvez 
utiliser le bouton droit de la souris pour d�finir un 
point de navigation directement sur un objet. Testez 
cette fonctionnalit� en zoomant sur le secteur SX 0, SY 0,
SZ 0 et clic droit sur le champ d'ast�ro�de du secteur.

-46
11
Bien. Veillez � ne pas engager la commande de saut imm�diatement
apr�s avoir plac� le point directement sur une plan�te, un
champ d'ast�ro�de ou une lune sous peine d'entrer en collision
avec eux. Si vous avez besoin de sauter � proximit� d'un de ces
objets pointez avec le clic droit puis � l'aide du clic gauche,
s�l�ctionnez un autre enmplacement � proximit�. Il faudra un 
peu de pratique avec la navigation pour �tre capable de sauter
� proximit� d'objets en toute s�curit�, prenez donc le temps
de bien int�grer cette fonctionnalit�. Un trac� s�r et pr�cis
sera important en cas de danger dans certaine r�gion hostile
de l'espace.

-47
13
La console de navigation fournie aussi des informations 
importantes sur le syst�me. Votre niveau de r�putation 
actuel est affich� sur le c�t� droit ainsi que l'�conomie
et la situation territorial. Les portes stellaires vous 
permettent de voyager entre les diff�rents syst�mes 
d'Evochron et sont affich�es en violet sur la carte. 
La destination des portes apparait lorsque que vous les 
pointez sur la carte. Les ic�nes des plan�tes fournissent 
aussi des informations comme le nom et le type d'�conomie 
qui vous permettent de savoir quels types d'objets peuvent 
�tre en forte demande. Il est important d'utiliser ces 
informations pour �tablir des routes commerciales 
efficaces.

-48
13
Pour survivre et connaitre le succ�s dans Evochron 
Mercenary, vous devez trouver de quoi vivre. Vous 
pouvez contracter toute sorte de missions, ou voler 
pour votre propre compte et remplir vos propres 
objectifs. Vous pouvez miner pour extraire 
des ressources de valeurs, faire du transport de 
marchandises ou de personnes, patrouiller contre les 
vaisseaux hostile, n�gocier avec les autres joueurs, 
explorer pour d�couvrir des b�n�fices cach�s, et 
encore beaucoup d'autres activit�s. Pour g�rer vos 
ressources et objets, la console d'inventaire est � 
votre disposition, touche F3 ou bouton INV. 
Ouvrez l'inventaire maintenant.

-49
15
La console d'inventaire vous permet de g�rer votre 
compte, votre carburant, les armes principales et 
secondaires mont�es sur le vaisseaux et le chargement 
de la soute. Lorsque vous �tes connect� � une station, 
une ville, ou un vaisseau m�re, vous pouvez utiliser 
cette console pour vous recharger en carburant, en
contre-mesures, et r�parer votre vaisseau. Lorsque vous 
�tes arrim�, vous pouvez aussi acc�der aux objets a 
vendre et aux contrats. Pointez sur un objet pour 
obtenir une description au milieu de la partie haute 
de la console. Pour acheter ou vendre un objet, 
cliquez simplement dessus. Les objets sont class�s 
selon un code couleur pour les identifier plus 
facilement. Les objets violet sont des marchandises,
les jaunes des �quipements, et les verts sont des armes.

-50
13
La console d'inventaire vous permet aussi de transf�rer
vos armes et �quipement depuis votre soute et de les 
installer. Pour transf�rer un objet sans le vendre, 
cliquez sur lui avec le bouton droit de la souris. Vous 
pouvez aussi acheter un objet et le placer dans votre 
soute en utilisant le bouton droit de la souris. Le 
bouton droit permet aussi de vendre une unit�s de 
ressources � la fois. Vous pouvez regrouper les 
objets de votre soute en utilisant la touche ALT 
tout en cliquant avec les boutons gauche ou droit 
de la souris. ALT clic gauche remplira au maximum des
capacit� de la soute pour le type de machandise 
s�l�ctionn�. Alt clic droit transf�re une seule unit�.

-51
16
La console d'inventaire donne aussi acc�s a d'autres 
consoles disponibles sur le cote gauche avec les 
boutons Crew Management, News Console et 
Flight/Event Log. La console d'�quipage vous permet 
d'embaucher des sp�cialistes qui vous feront profiter 
de leurs comp�tences et augmenterons les possibilit�s 
de votre vaisseau. Vous pouvez aussi licencier votre 
�quipage et g�rer leurs salaires. La console de News 
fournie des informations importantes sur le syst�me, 
incluant l'�tat du march� dans la zone. Vous y trouverez
�galement des d�tails concernant votre r�putation, les 
statistiques pilotes et les informations du syst�me local.
Le journal Flight/Event affiche les d�tails des contrats,
articles vendu et achet� et plusieurs autre type d'�l�ments.
Vous pouvez �galment trier la liste par cat�gorie afin
d'examiner des entr�es sp�cifique.

-52
11
Vous pouvez commercer directement avec les autres 
vaisseaux si vous avez une bonne r�putation avec eux.
Vous ne gagnerez pas forc�ment plus avec eux, mais 
c'est une fa�on simple et rapide de faire du commerce. 
Pour envoyer une requ�te de commerce a un vaisseau, 
s�lectionnez le puis appuyer sur la touche F4 ou 
cliquez sur le bouton TRADE. Si le vaisseau 
accepte la requ�te, la console de commerce s'ouvre.
�tant donn� qu'aucun navire ne se trouve � proximit�
dans ce tutoriel, la console du commerce s'ouvre 
automatiquement cette fois ci.

-53
14
Si l'autre pilote a quelques choses � vous proposer, 
cela apparaitra sur la partie droite de la console 
sous Items Being Offered:. Le prix du march� local 
s'affichera � c�t� de l'objet. Si vous ajoutez des 
objets, l'autre pilote proposera un montant pour la 
transaction. Cliquez sur Submit pour envoyer 
votre offre. Si l'autre pilote accepte la transaction 
elle sera finalis�e. Vous pouvez envoyez de multiple 
requ�tes. Vous pouvez �galement utiliser cette console 
pour soudoyer les vaisseau hostile, embaucher des vaisseaux
dans votre flotte, et demander des informations sur d'autres
pilotes. En multi-joueur vous pouvez vous lier � un autre 
joueur comme op�rateur de tourelle, envoyer un d�fi de course,
et arranger un contrat entre joueur.

-54
12
Lorsque vous interagissez avec les vaisseaux alentours, 
cela affecte votre r�putation. Vous pouvez rendre un ami 
hostile en l'attaquant et inversement en proposant de 
l'argent. La r�putation et un syst�me complexe qui 
affectera l'hostilit� des autres vaisseaux, le montant 
des taxes, des contrats proposes et des prix de vente. 
Votre r�putation �voluera aussi lorsque vous remplirez
des missions pour diverses factions. Un changement de 
votre niveau de r�putation sera indiqu� dans la fen�tre 
de message, l� o� s'affiche le texte actuel.
Votre niveau de r�putation avec les factions locales 
s'affiche dans la Console de News.

-55
16
Dans cette �tape, nous allons voir comment s'arrimer � une 
station. Pour cela il suffit de voler vers la zone d'arrimage 
en haut de la station. La zone d'arrimage est prot�g�e par 
un bouclier atmosph�rique bleu et des lumi�res d'approche 
clignotantes indique le bon angle d'approche. Chaque porte
est num�rot�e de 1 � 5 et lorque vous vous en approcherez,
le contr�leur de la station d'arrimage vous informera si la 
porte la plus proche est d�gag�e ou si il y a du trafic sur 
la trajectoire de vol. Votre HUD affiche �galement une aide 
� l'arrimage. Le chemin sera indique en rouge si votre angle 
d'approche est incorrecte ou en vert dans le cas contraire. 
Lorsque vous serez suffisamment proche de la zone d'arrimage, 
un rayon tracteur prendra le relai et vous arrimera automatiquement.
Votre console d'inventaire s'affichera alors. Pour vous d�sarrimer, 
il suffit de fermer la console d'inventaire. Entrainez-vous 
� l'arrimage avec cette station maintenant.

-56
2
Fermez la console d'inventaire pour vous d�sarrimer et 
d�sactiver le rayon tracteur.

-57
13
Pour cette �tape, vous apprendrez comment utiliser le
faisceau de minage pour r�cup�rer les m�tieres pr�cieuse
des ast�ro�des. Le faisceau de minage (Mining Beam) peut
�galement �tre utilis� pour r�cup�rer la mati�re des
plan�tes, des grottes d'ast�ro�de, des n�buleuses, des 
�toiles et d'effectuer des op�rations de nettoyage.
Pour pratiquer l'exploitation mini�re, approchez votre
vaisseau � moins de 150 m�tres et arr�tez vous. Assurez
vous d'utiliser l'affichage complet du HUD avec la touche 
H afin d'avoir les distances indiqu�es. Une fois que vous 
�tes � port�e utiliser le rayon en maintenant la touche B
ou en le v�rrouillant avec ALT-B. Laissez le rayon acitf
jusqu'� ce que vous r�cup�rez 5 unit� de mat�riau.

-58
13
Le syst�me de gestion du vaisseau trie automatiquement
les mat�riaux dans la soute. Si vous n'avez pas de place
pour un autre type de mat�riau, il ne sera pas stock�.
Si vous envisagez l'exploitation mini�re vous aurez la 
possibilit� de modifier votre vaisseau pour lui permettre
de stocker plusieurs type de mat�riau. Vous pouvez aussi
laisser une unit� de mati�re par emplacement afin que
le syst�me ne mine que cette ressource. Vous pouvez 
acc�lerer l'exploitation en d�pla�ant le rayon autour de 
l'objet plut�t que de le laisser sur une seul position.
Vous pouvez �galement engager un membre d'�quipage sp�cialis�
(science-ops) qui contribuera � am�liorer la vitesse 
d'exploitation.



-59
14
Pour la session suivante, il vous sera demand� de 
descendre � travers l'atmosph�re d'une plan�te et de
vous poser sur la ville. Pour atterrir, baissez votre 
vitesse en dessous de 1200 MPS avant d'entrer en 
atmosph�re et aidez-vous de l'aide � l'atterrissage du 
HUD. Si vous �tes loin de la ville, l'aide a 
l'atterrissage sera rouge. D�s que vous serez � proximit�, 
elle deviendra verte et pointera vers la station. 
Volez jusqu'� la partie haute de la station d'arrimage. 
Comme pr�c�demment un rayon tracteur assurera un arrimage 
facile. Pensez � couper vos moteurs pour �viter une consommation 
inutile. Pour vous d�sarrimer, fermer la console d'inventaire 
(F3). Mettez les gaz pour un d�part s�r et pouvoir quitter le sol de 
la plan�te. Exercez-vous sur cette plan�te.

-60
2
Fermer la console d'inventaire pour d�sactiver le 
rayon tracteur.

-61
13
Cela peut �tre un v�ritable d�fi de r�ussir lorsque l'on 
d�bute. Voici quelques conseils pour gagner de l'argent. 
Avec quelques am�liorations � faible cout, vous pourrez 
am�liorer votre vaisseau et participer � des courses qui 
paies bien avec peu de risque. Si vous pilotez un vaisseau 
civil �quip� d'un rayon tracteur/extracteur, vous pourrez 
vous lancer dans l'exploitation mini�re en extrayant des 
mati�res premi�res des ast�ro�des. Le nettoyage de panneau 
solaire peut aussi �tre lucratif, et est particuli�rement 
sans risque. Lorsque vous aurez amass� suffisamment d'argent,
vous pourrez am�liorer votre vaisseau et acc�der 
� des contrats plus lucratifs. Vous allez maintenant �tre 
redirig� vers le menu principal.

-62
1
Bonne chance!
